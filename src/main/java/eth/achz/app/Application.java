package eth.achz.app;

import eth.achz.app.repository.impl.CourseRepositoryImpl;
import eth.achz.app.repository.impl.StudentRepositoryImpl;
import eth.achz.app.repository.impl.TeacherRepositoryImpl;
import eth.achz.app.resource.CourseResource;
import eth.achz.app.resource.StudentResource;
import eth.achz.app.resource.TeacherResource;
import eth.achz.app.resource.impl.CourseResourceImpl;
import eth.achz.app.resource.impl.StudentResourceImpl;
import eth.achz.app.resource.impl.TeacherResourceImpl;
import eth.achz.app.service.CourseService;
import eth.achz.app.service.StudentService;
import eth.achz.app.service.TeacherService;
import eth.achz.app.service.impl.CourseServiceImpl;
import eth.achz.app.service.impl.StudentServiceImpl;
import eth.achz.app.service.impl.TeacherServiceImpl;
import eth.achz.app.util.DBWorker;

public class Application {

    public DBWorker dbWorker;

    public StudentService studentService = new StudentServiceImpl(new StudentRepositoryImpl());
    public TeacherService teacherService = new TeacherServiceImpl(new TeacherRepositoryImpl());
    public CourseService courseService = new CourseServiceImpl(new CourseRepositoryImpl());

    public StudentResource studentResource = new StudentResourceImpl(studentService);
    public TeacherResource teacherResource = new TeacherResourceImpl(teacherService);
    public CourseResource courseResource = new CourseResourceImpl(courseService);

    public Application init() {
        Application app = new Application();
        app.dbWorker = new DBWorker(app);
        return app;
    }
}
