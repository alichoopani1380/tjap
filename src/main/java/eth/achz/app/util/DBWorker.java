package eth.achz.app.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import eth.achz.app.Application;
import eth.achz.app.model.Course;
import eth.achz.app.model.Student;
import eth.achz.app.model.Teacher;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;

public class DBWorker {
    Application app;

    public DBWorker(Application app) {
        this.app = app;
    }

    public void loadFromFile(String fileName) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        try {

            JsonNode root = mapper.readTree(Paths.get("src/main/resources/db.json").toFile());

            ArrayNode students = (ArrayNode) root.at("/students");
            students.forEach(s -> app.studentService.insert(mapper.convertValue(s, Student.class)));

            ArrayNode teachers = (ArrayNode) root.at("/teachers");
            teachers.forEach(s -> app.teacherService.insert(mapper.convertValue(s, Teacher.class)));

            ArrayNode courses = (ArrayNode) root.at("/courses");
            courses.forEach(s -> app.courseService.insert(mapper.convertValue(s, Course.class)));

//            System.out.println(db);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
