package eth.achz.app.util;

public class CommandLineUtils {
    public static void printInBox(String s) {
        StringBuilder border = new StringBuilder();
        border.append("-".repeat(s.length() + 6));
        System.out.println(border);
        System.out.println("|  " + s + "  |");
        System.out.println(border);
    }

    public static void invalidInput() {
        System.out.println("!!!   invalid input   !!!");
    }

    public static void warning(String s) {
        printInBox("!!! " + s + " !!!");
    }
}
