package eth.achz.app.repository.impl;

import eth.achz.app.model.Course;
import eth.achz.app.repository.CourseRepository;

public class CourseRepositoryImpl extends BaseRepositoryImpl<Course> implements CourseRepository {
}
