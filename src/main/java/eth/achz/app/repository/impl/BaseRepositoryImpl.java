package eth.achz.app.repository.impl;

import eth.achz.app.model.BaseModel;
import eth.achz.app.repository.BaseRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class BaseRepositoryImpl<T extends BaseModel> implements BaseRepository<T> {

    Integer lastId = 0;

    Map<Integer, T> repo = new HashMap<>();

    @Override
    public Integer incrementAndGetLastId() {
        return ++lastId;
    }

    @Override
    public Optional<T> getById(Integer id) {
        return Optional.ofNullable(repo.get(id));
    }

    @Override
    public T  insert(T object) {
        var id = incrementAndGetLastId();
        object.setId(id);
        repo.put(id, object);
        return object;
    }

    @Override
    public T insertById(Integer id, T object) {
        object.setId(id);
        repo.put(id, object);
        return object;
    }

    @Override
    public T updateById(Integer id, T object) {
        //TODO
        object.setId(id);
        return repo.put(id, object);
    }

    @Override
    public Map<Integer, T> getAll() {
        return repo;
    }
}
