package eth.achz.app.repository.impl;

import eth.achz.app.model.Student;
import eth.achz.app.repository.StudentRepository;

import java.util.Collections;
import java.util.Comparator;

public class StudentRepositoryImpl extends BaseRepositoryImpl<Student> implements StudentRepository {

    public Integer getLastRegisterCounter() {
        if (repo.isEmpty()) return 0;
        return Collections.max(repo.values(), Comparator.comparing(Student::getRegisterCounter)).getRegisterCounter();
    }

    @Override
    public Student insert(Student object) {
        object.setRegisterCounter(getLastRegisterCounter() + 1);
        return super.insert(object);
    }
}
