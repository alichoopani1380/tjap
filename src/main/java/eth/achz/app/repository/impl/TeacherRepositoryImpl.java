package eth.achz.app.repository.impl;

import eth.achz.app.model.Teacher;
import eth.achz.app.repository.TeacherRepository;

import java.util.Collections;
import java.util.Comparator;

public class TeacherRepositoryImpl extends BaseRepositoryImpl<Teacher> implements TeacherRepository {

    public Integer getLastRegisterCounter() {
        if (repo.isEmpty()) return 0;
        return Collections.max(repo.values(), Comparator.comparing(Teacher::getRegisterCounter)).getRegisterCounter();
    }

    @Override
    public Teacher insert(Teacher object) {
        object.setRegisterCounter(getLastRegisterCounter() + 1);
        return super.insert(object);
    }
}
