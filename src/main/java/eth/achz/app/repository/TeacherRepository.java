package eth.achz.app.repository;

import eth.achz.app.model.Teacher;

public interface TeacherRepository extends BaseRepository<Teacher> {

}
