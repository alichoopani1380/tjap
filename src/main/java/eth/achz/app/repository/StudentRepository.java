package eth.achz.app.repository;

import eth.achz.app.model.Student;

public interface StudentRepository extends BaseRepository<Student> {
}
