package eth.achz.app.repository;

import eth.achz.app.model.BaseModel;

import java.util.Map;
import java.util.Optional;

public interface BaseRepository<T extends BaseModel> {

    Integer incrementAndGetLastId();

    Optional<T> getById(Integer id);
    T insert(T object);
    T insertById(Integer id, T object);
    T updateById(Integer id, T object);
    Map<Integer, T> getAll();
}
