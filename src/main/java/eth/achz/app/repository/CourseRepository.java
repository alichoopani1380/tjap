package eth.achz.app.repository;

import eth.achz.app.model.Course;

public interface CourseRepository extends BaseRepository<Course> {

}
