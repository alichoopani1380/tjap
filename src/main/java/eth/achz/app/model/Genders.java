package eth.achz.app.model;

public enum Genders {
    Male("1"),
    Female("2"),
    Unknown("3");

    private final String value;

    Genders(String i) {
        value = i;
    }

    public Genders translate(int str) {
        return switch (str) {
            case 1 -> Genders.valueOf("Male");
            case 2 -> Genders.valueOf("Female");
            default -> Genders.valueOf("Unknown");
        };
    }

    public String getValue() {
        return value;
    }
}
