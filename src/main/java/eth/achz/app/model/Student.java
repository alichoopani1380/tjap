package eth.achz.app.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
public class Student extends Person {

    public String studentNumber;
    public AcademicBranches branch = AcademicBranches.Other;
    public AcademicSubBranches subBranch = AcademicSubBranches.Other;
    public Integer enterYear;
    public Genders gender = Genders.Unknown;
    public StudyTypes studyType = StudyTypes.Daily; //daily -> 1 or nightly -> 2
    public Integer registerCounter;

    public String getStudentNumber() {
        return getEnterYear().toString()
                + getBranch().getValue()
                + getSubBranch().getValue()
                + getGender().getValue()
                + getStudyType().getValue()
                + getRegisterCounter().toString();
    }

    public String getStudentId() {
        return getEnterYear().toString() + getRegisterCounter().toString();
    }

    public String toString() {
        return "(id: " + getId() + " | " +
                "name: " + getFullName() + " | " +
                "student number: " + getStudentNumber() + " | " +
                "branch: " + getBranch() + ")";
    }

}
