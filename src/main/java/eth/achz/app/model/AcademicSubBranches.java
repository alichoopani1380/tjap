package eth.achz.app.model;

public enum AcademicSubBranches {
    Telecommunications("01"),
    Power("02"),
    Electronic("03"),
    Control("04"),
    ManufacturingEngineering("05"),
    HardWare("06"),
    IT("07"),
    Fluids("08"),
    Solids("09"),
    SoftWare("10"),
    Other("00");

    private final String value;

    AcademicSubBranches(String i) {
        value = i;
    }

    public AcademicSubBranches translate(int str) {
        return switch (str) {
            case 1 -> AcademicSubBranches.valueOf("Telecommunications");
            case 2 -> AcademicSubBranches.valueOf("Power");
            case 3 -> AcademicSubBranches.valueOf("Electronic");
            case 4 -> AcademicSubBranches.valueOf("Control");
            case 5 -> AcademicSubBranches.valueOf("ManufacturingEngineering");
            case 6 -> AcademicSubBranches.valueOf("HardWare");
            case 7 -> AcademicSubBranches.valueOf("IT");
            case 8 -> AcademicSubBranches.valueOf("Fluids");
            case 9 -> AcademicSubBranches.valueOf("Solids");
            case 10 -> AcademicSubBranches.valueOf("SoftWare");
            default -> AcademicSubBranches.valueOf("Other");
        };
    }

    public String getValue() {
        return value;
    }
}
