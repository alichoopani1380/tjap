package eth.achz.app.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@NoArgsConstructor
@Getter
@Setter
public class Teacher extends Person {
    public Integer enterYear;
    public Genders gender;
    public Integer registerCounter;
    public AcademicBranches branch;

    public String toString() {
        return "(id: " + getId() + " | " +
                "name: " + getFullName() + " | " +
                "branch: " + getBranch() + ")";
                
    }
}
