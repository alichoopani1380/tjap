package eth.achz.app.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@NoArgsConstructor
@Getter
@Setter
@ToString
public abstract class Person extends BaseModel {
    public String firstName;
    public String lastName;
    public String nationalCode;
    public String birthDate;
    public String phoneNumber;

    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }
}
