package eth.achz.app.model;

public enum StudyTypes {
    Daily("01"), Nightly("02"), Unknown("03");

    private final String value;

    StudyTypes(String i) {
        value = i;
    }

    public StudyTypes translate(int str) {
        return switch (str) {
            case 1 -> StudyTypes.valueOf("Daily");
            case 2 -> StudyTypes.valueOf("Nightly");
            default -> StudyTypes.valueOf("Unknown");
        };
    }

    public String getValue() {
        return value;
    }
}
