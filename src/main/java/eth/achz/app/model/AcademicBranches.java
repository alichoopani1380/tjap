package eth.achz.app.model;

public enum AcademicBranches {
    Computer("1"), Electric("2"), Mechanic("3"),  Other("0");

    private final String value;

    AcademicBranches(String i) {
        value = i;
    }

    public AcademicBranches translate(int str) {
        return switch (str) {
            case 1 -> AcademicBranches.valueOf("Computer");
            case 2 -> AcademicBranches.valueOf("Electric");
            case 3 -> AcademicBranches.valueOf("Mechanic");
            default -> AcademicBranches.valueOf("Other");
        };
    }

    public String getValue() {
        return value;
    }
}
