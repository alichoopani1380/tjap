package eth.achz.app.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

@AllArgsConstructor
@SuperBuilder
@Getter
@Setter
@Builder
@NoArgsConstructor
@ToString
public class Course extends BaseModel {
    protected String title;
    protected Integer code;
    protected Integer practicalUnits;
    protected Integer theoreticalUnits;

    public String toString() {
        return "(id: " + getId() + " | " +
                "title: " + getTitle() + " | " +
                "code: " + getCode() + " | " +
                "practical units: " + getPracticalUnits() + " | " +
                "theoretical units: " + getTheoreticalUnits() + ")";
    }
}
