package eth.achz.app.resource.impl;

import eth.achz.app.model.BaseModel;
import eth.achz.app.resource.BaseResource;
import eth.achz.app.service.BaseService;

public class BaseResourceImpl<T extends BaseModel> implements BaseResource<T> {
    public BaseService<T> service;

    public BaseResourceImpl(BaseService<T> service) {
        this.service = service;
    }

    @Override
    public void printAll() {
        service.getAll().forEach((key, value) -> System.out.println("id: " + key + " -> " + "data:[ " + value + " ]"));
    }

    @Override
    public void add() {
        System.out.println("NOT IMPLEMENTED");
    }

    @Override
    public void updateById() {
        System.out.println("NOT IMPLEMENTED");
    }

    @Override
    public void searchById() {
        System.out.println("NOT IMPLEMENTED");
    }
}
