package eth.achz.app.resource.impl;

import eth.achz.app.model.*;
import eth.achz.app.resource.StudentResource;
import eth.achz.app.service.StudentService;
import eth.achz.app.util.CommandLineUtils;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Scanner;

public class StudentResourceImpl extends BaseResourceImpl<Student> implements StudentResource {

    public StudentResourceImpl(StudentService service) {
        super(service);
    }

    @Override
    public void add() {
        Scanner sc = new Scanner(System.in);

        System.out.println("insert student first name: ");
        var firstName = sc.nextLine();

        System.out.println("insert student last name: ");
        var lastName = sc.nextLine();

        System.out.println("insert student national code: ");
        var nationalCode = sc.nextLine();

        System.out.println("insert student birthdate: ");
        var birthDate = sc.nextLine();

        System.out.println("insert student phone number: ");
        var phoneNumber = sc.nextLine();

        System.out.println("insert student enter year: ");
        var enterYear = sc.nextInt();

        System.out.println("insert student branch: ");
        System.out.println("|- Computer:   1");
        System.out.println("|- Electronic: 2");
        System.out.println("|- Mechanic:   3");
        System.out.println("|- Other:      0");
        var branch = sc.nextInt();

        System.out.println("insert student sub branch: ");
        System.out.println("|- Telecommunications:       1");
        System.out.println("|- Power:                    2");
        System.out.println("|- Electronic:               3");
        System.out.println("|- Control:                  4");
        System.out.println("|- ManufacturingEngineering: 5");
        System.out.println("|- HardWare:                 6");
        System.out.println("|- IT:                       7");
        System.out.println("|- Fluids:                   8");
        System.out.println("|- Solids:                   9");
        System.out.println("|- SoftWare:                 10");
        System.out.println("|- Other:                    0");
        var subBranch = sc.nextInt();

        System.out.println("insert student gender: ");
        System.out.println("|- Male:    1");
        System.out.println("|- Female:  2");
        System.out.println("|- Unknown: 3");
        var gender = sc.nextInt();

        System.out.println("insert student study type(daily/nightly): ");
        System.out.println("|- Daily:   1");
        System.out.println("|- Nightly: 2");
        System.out.println("|- Unknown: 3");
        var studyType = sc.nextInt();

        service.insert(
                Student.builder()
                        .firstName(firstName)
                        .lastName(lastName)
                        .nationalCode(nationalCode)
                        .birthDate(birthDate)
                        .phoneNumber(phoneNumber)
                        .enterYear(enterYear)
                        .branch(AcademicBranches.Other.translate(branch))
                        .subBranch(AcademicSubBranches.Other.translate(subBranch))
                        .gender(Genders.Unknown.translate(gender))
                        .studyType(StudyTypes.Unknown.translate(studyType))
                        .build()
        );
        CommandLineUtils.printInBox(" -- student inserted! -- ");

    }

    @Override
    public void updateById() {
        Scanner sc = new Scanner(System.in);
        System.out.println("List of all students.");
        this.printAll();
        System.out.println("please insert student id: ");
        Integer id = sc.nextInt();

        Optional<Student> st = service.getById(id);
        if (st.isEmpty()) {
            CommandLineUtils.warning("please insert valid id");
            return;
        }
        Student student = st.get();

        System.out.println("insert student new first name: ");
        var in = sc.nextLine();
        var firstName = Objects.equals(in, "") ? student.firstName : in;

        System.out.println("insert student last name: ");
        in = sc.nextLine();
        var lastName = Objects.equals(in, "") ? student.lastName : in;

        System.out.println("insert student national code: ");
        in = sc.nextLine();
        var nationalCode = Objects.equals(in, "") ? student.nationalCode : in;

        System.out.println("insert student birthdate: ");
        in = sc.nextLine();
        var birthDate = Objects.equals(in, "") ? student.birthDate : in;

        System.out.println("insert student phone number: ");
        in = sc.nextLine();
        var phoneNumber = Objects.equals(in, "") ? student.phoneNumber : in;

        System.out.println("insert student enter year: ");
        var intIn = sc.nextInt();
        var enterYear = Objects.equals(in, "") ? student.enterYear : intIn;

        System.out.println("insert student branch: ");
        System.out.println("|- Computer:   1");
        System.out.println("|- Electric: 2");
        System.out.println("|- Mechanic:   3");
        System.out.println("|- Other:      0");
        intIn = sc.nextInt();
        var branch = Objects.equals(intIn, "") ? student.branch : AcademicBranches.Other.translate(intIn);

        System.out.println("insert student sub branch: ");
        System.out.println("|- Telecommunications:       1");
        System.out.println("|- Power:                    2");
        System.out.println("|- Electronic:               3");
        System.out.println("|- Control:                  4");
        System.out.println("|- ManufacturingEngineering: 5");
        System.out.println("|- HardWare:                 6");
        System.out.println("|- IT:                       7");
        System.out.println("|- Fluids:                   8");
        System.out.println("|- Solids:                   9");
        System.out.println("|- SoftWare:                 10");
        System.out.println("|- Other:                    0");
        intIn = sc.nextInt();
        var subBranch = Objects.equals(intIn, "") ? student.subBranch : AcademicSubBranches.Other.translate(intIn);

        System.out.println("insert student gender: ");
        System.out.println("|- Male:    1");
        System.out.println("|- Female:  2");
        System.out.println("|- Unknown: 3");
        intIn = sc.nextInt();
        var gender = Objects.equals(intIn, "") ? student.gender : Genders.Unknown.translate(intIn);

        System.out.println("insert student study type(daily/nightly): ");
        System.out.println("|- Daily:   1");
        System.out.println("|- Nightly: 2");
        System.out.println("|- Unknown: 3");
        intIn = sc.nextInt();
        var studyType = Objects.equals(intIn, "") ? student.studyType : StudyTypes.Unknown.translate(intIn);

        student.setFirstName(firstName);
        student.setLastName(lastName);
        student.setNationalCode(nationalCode);
        student.setBirthDate(birthDate);
        student.setPhoneNumber(phoneNumber);
        student.setEnterYear(enterYear);
        student.setSubBranch(subBranch);
        student.setBranch(branch);
        student.setGender(gender);
        student.setStudyType(studyType);

        service.updateById(id, student);
        CommandLineUtils.printInBox(" -- student updated! -- ");

    }

    public void searchById() {
        Scanner sc = new Scanner(System.in);
        System.out.println("please insert id: ");
        Integer in = sc.nextInt();
        Optional<Student> st = service.getById(in);
        if (st.isEmpty()) {
            CommandLineUtils.warning("student not fount");
        } else {
            System.out.println(st.get());
        }
    }

    @Override
    public void searchByStudentNumber() {
        Scanner sc = new Scanner(System.in);
        System.out.println("please insert student number: ");
        String in = sc.nextLine();
        Optional<Student> student = ((StudentService) service).getByStudentNumber(in);
        if (student.isEmpty()) {
            CommandLineUtils.warning("student not fount");
        } else {
            System.out.println(student.get());
        }
    }

    @Override
    public void searchByBranch() {
        Scanner sc = new Scanner(System.in);
        System.out.println("insert student branch: ");
        System.out.println("|- Computer:   1");
        System.out.println("|- Electronic: 2");
        System.out.println("|- Mechanic:   3");
        System.out.println("|- Other:      0");
        var intIn = sc.nextInt();
        var branch = AcademicBranches.Other.translate(intIn);

        List<Student> studentList = ((StudentService) service).getAllByBranch(branch);
        if (studentList.isEmpty()) {
            CommandLineUtils.warning("there is no student in this branch");
        } else {
            System.out.println(studentList);
        }

    }

    @Override
    public void searchByName() {
        Scanner sc = new Scanner(System.in);
        System.out.println("please insert student name: ");
        String in = sc.nextLine();
        List<Student> students = ((StudentService) service).getAllByMatchName(in);
        if (students.isEmpty()) {
            CommandLineUtils.warning("no name matched");
        } else {
            System.out.println(students);
        }
    }
}
