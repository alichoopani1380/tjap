package eth.achz.app.resource.impl;

import eth.achz.app.model.AcademicBranches;
import eth.achz.app.model.Genders;
import eth.achz.app.model.Teacher;
import eth.achz.app.resource.TeacherResource;
import eth.achz.app.service.TeacherService;
import eth.achz.app.util.CommandLineUtils;

import java.util.Scanner;

public class TeacherResourceImpl extends BaseResourceImpl<Teacher> implements TeacherResource {
    public TeacherResourceImpl(TeacherService service) {
        super(service);
    }

    @Override
    public void add() {
        Scanner sc = new Scanner(System.in);

        System.out.println("insert teacher first name: ");
        var firstName = sc.nextLine();

        System.out.println("insert teacher last name: ");
        var lastName = sc.nextLine();

        System.out.println("insert teacher national code: ");
        var nationalCode = sc.nextLine();

        System.out.println("insert teacher birthdate: ");
        var birthDate = sc.nextLine();

        System.out.println("insert teacher phone number: ");
        var phoneNumber = sc.nextLine();

        System.out.println("insert teacher enter year: ");
        var enterYear = sc.nextInt();

        System.out.println("insert teacher gender: ");
        System.out.println("|- Male:    1");
        System.out.println("|- Female:  2");
        System.out.println("|- Unknown: 3");
        var gender = sc.nextInt();

        System.out.println("insert student branch: ");
        System.out.println("|- Computer:   1");
        System.out.println("|- Electric: 2");
        System.out.println("|- Mechanic:   3");
        System.out.println("|- Other:      0");
        var branch = sc.nextInt();

        service.insert(
                Teacher.builder()
                        .firstName(firstName)
                        .lastName(lastName)
                        .nationalCode(nationalCode)
                        .birthDate(birthDate)
                        .phoneNumber(phoneNumber)
                        .enterYear(enterYear)
                        .gender(Genders.Unknown.translate(gender))
                        .branch(AcademicBranches.Other.translate(branch))
                        .build()
        );
        CommandLineUtils.printInBox(" -- teacher inserted! -- ");
    }
}
