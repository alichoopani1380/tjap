package eth.achz.app.resource.impl;

import eth.achz.app.model.Course;
import eth.achz.app.resource.CourseResource;
import eth.achz.app.service.CourseService;
import eth.achz.app.util.CommandLineUtils;

import java.util.Scanner;

public class CourseResourceImpl extends BaseResourceImpl<Course> implements CourseResource {

    public CourseResourceImpl(CourseService service) {
        super(service);
    }

    @Override
    public void add() {
        Scanner sc = new Scanner(System.in);

        System.out.println("insert course title: ");
        var title = sc.nextLine();

        System.out.println("insert course code: ");
        var code = sc.nextInt();

        System.out.println("insert course practical units: ");
        var practicalUnits = sc.nextInt();

        System.out.println("insert student national code: ");
        var theoreticalUnits = sc.nextInt();

        service.insert(
                Course.builder()
                        .title(title)
                        .code(code)
                        .practicalUnits(practicalUnits)
                        .theoreticalUnits(theoreticalUnits)
                        .build()
        );

        CommandLineUtils.printInBox(" -- student inserted! -- ");
    }
}
