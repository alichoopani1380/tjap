package eth.achz.app.resource;

import eth.achz.app.model.Teacher;

public interface TeacherResource extends BaseResource<Teacher> {
}
