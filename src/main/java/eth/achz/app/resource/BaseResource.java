package eth.achz.app.resource;

import eth.achz.app.model.BaseModel;

public interface BaseResource<T extends BaseModel> {
    void printAll();
    void add();
    void updateById();
    void searchById();
}
