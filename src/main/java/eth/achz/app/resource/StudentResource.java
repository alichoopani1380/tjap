package eth.achz.app.resource;

import eth.achz.app.model.Student;

public interface StudentResource extends BaseResource<Student> {
    void searchByStudentNumber();

    void searchByBranch();

    void searchByName();
}
