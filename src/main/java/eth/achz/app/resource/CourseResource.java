package eth.achz.app.resource;

import eth.achz.app.model.Course;

public interface CourseResource extends BaseResource<Course> {

}
