package eth.achz.app.service;

import eth.achz.app.model.Teacher;

public interface TeacherService extends BaseService<Teacher> {
}
