package eth.achz.app.service;

import java.util.Map;
import java.util.Optional;

public interface BaseService<T> {
    Map<Integer, T> getAll();
    void insert(T object);
    void updateById(Integer id, T object);
    Optional<T> getById(Integer id);
}
