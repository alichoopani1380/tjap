package eth.achz.app.service;

import eth.achz.app.model.AcademicBranches;
import eth.achz.app.model.Student;

import java.util.List;
import java.util.Optional;

public interface StudentService extends BaseService<Student> {
    Optional<Student> getByStudentNumber(String in);

    List<Student> getAllByBranch(AcademicBranches branch);

    List<Student> getAllByMatchName(String in);
}
