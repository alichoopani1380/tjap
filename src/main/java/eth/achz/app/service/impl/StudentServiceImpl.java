package eth.achz.app.service.impl;

import eth.achz.app.model.AcademicBranches;
import eth.achz.app.model.Student;
import eth.achz.app.repository.StudentRepository;
import eth.achz.app.service.StudentService;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class StudentServiceImpl extends BaseServiceImpl<Student> implements StudentService {

    public StudentServiceImpl(StudentRepository repo) {
        super(repo);
    }

    @Override
    public Optional<Student> getByStudentNumber(String studentNumber) {
        return repository.getAll().values().stream()
                .filter(student -> Objects.equals(student.getStudentNumber(), studentNumber))
                .findFirst();
    }

    @Override
    public List<Student> getAllByBranch(AcademicBranches branch) {
        return repository.getAll().values().stream()
                .filter(student -> student.branch == branch)
                .collect(Collectors.toList());
    }

    @Override
    public List<Student> getAllByMatchName(String in) {
        return repository.getAll().values().stream()
                .filter(student -> student.getFullName().contains(in))
                .collect(Collectors.toList());
    }
}
