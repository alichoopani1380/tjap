package eth.achz.app.service.impl;

import eth.achz.app.model.BaseModel;
import eth.achz.app.repository.BaseRepository;
import eth.achz.app.service.BaseService;

import java.util.Map;
import java.util.Optional;

public class BaseServiceImpl<T extends BaseModel> implements BaseService<T> {

    BaseRepository<T> repository;

    public BaseServiceImpl(BaseRepository<T> repository) {
        this.repository = repository;
    }

    @Override
    public Map<Integer, T> getAll() {
        return  repository.getAll();
    }

    @Override
    public void insert(T object) {
        repository.insert(object);
    }

    @Override
    public void updateById(Integer id, T object) {
        repository.updateById(id, object);
    }

    @Override
    public Optional<T> getById(Integer id) {
        return repository.getById(id);
    }
}
