package eth.achz.app.service.impl;

import eth.achz.app.model.Course;
import eth.achz.app.repository.CourseRepository;
import eth.achz.app.service.CourseService;

public class CourseServiceImpl extends BaseServiceImpl<Course> implements CourseService {
    public CourseServiceImpl(CourseRepository repo) {
        super(repo);
    }
}
