package eth.achz.app.service.impl;

import eth.achz.app.model.Teacher;
import eth.achz.app.repository.TeacherRepository;
import eth.achz.app.service.TeacherService;

public class TeacherServiceImpl extends BaseServiceImpl<Teacher> implements TeacherService {
    public TeacherServiceImpl(TeacherRepository repo) {
        super(repo);
    }
}
