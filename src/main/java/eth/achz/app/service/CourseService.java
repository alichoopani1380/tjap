package eth.achz.app.service;

import eth.achz.app.model.Course;

public interface CourseService extends BaseService<Course> {
}
