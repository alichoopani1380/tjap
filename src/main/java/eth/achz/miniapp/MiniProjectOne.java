package eth.achz.miniapp;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eth.achz.app.util.CommandLineUtils;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class MiniProjectOne {
    public static void init() {
        Scanner sc = new Scanner(System.in);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        System.out.println("pleas insert file path(or Enter to choose default file): ");
        var in = sc.nextLine();

        var filePath = in.equals("") ? "src/main/resources/miniProjectOne.json" : in;

        try {
            JsonNode root = mapper.readTree(Paths.get(filePath).toFile());
            ArrayList<Integer> scoreList = mapper.readValue(Paths.get(filePath).toFile(), ArrayList.class);
            System.out.println("students score list: " + scoreList);
            var average = scoreList.stream().mapToDouble(d -> d).average().getAsDouble();
            System.out.println("average: " + average);

            var newScoreList = scoreList.stream()
                    .map(x -> x + average > 20 ? 20 : x + average).toList();

            System.out.println("students new score list: " + newScoreList);

        } catch (IOException e) {
            CommandLineUtils.warning("file not found");
        }

    }
}
