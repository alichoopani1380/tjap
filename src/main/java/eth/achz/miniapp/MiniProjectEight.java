package eth.achz.miniapp;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import eth.achz.app.util.CommandLineUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class MiniProjectEight {
    public static void init() {
        Scanner sc = new Scanner(System.in);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        System.out.println("pleas insert file path(or Enter to choose default file): ");
        var in = sc.nextLine();

        var filePath = in.equals("") ? "src/main/resources/miniProjectEight.json" : in;

        try {
            JsonNode root = mapper.readTree(Paths.get(filePath).toFile());
            List<Employee> employeeList = new ArrayList<>();
            ArrayNode employees = (ArrayNode) root.at("/employees");
            employees.forEach(s -> employeeList.add(mapper.convertValue(s, Employee.class)));

            double maxReceived = 0;
            int maxReceivedPersonalCode = 0;
            for (int i = 0; i < (long) employeeList.size(); i++) {
                Employee e = employeeList.get(i);
                System.out.println("personal code: " + e.getPersonalCode() + " | salary: " + e.getSalary() + " | tax: " + e.getTax());
                if (e.getReceived() > maxReceived){
                    maxReceived = e.getReceived();
                    maxReceivedPersonalCode = e.getPersonalCode();
                }
            }

            System.out.println("max received : [personal code: " + maxReceivedPersonalCode + ", received:" + maxReceived + "]");


        } catch (IOException e) {
            CommandLineUtils.warning("file not found");
        }
    }
}

@Getter
@Setter
@NoArgsConstructor
class Employee {
    Integer personalCode;
    Integer salary;

    public double getTax() {
        if (getSalary() < 4000000) {
            return 0;
        } else if (getSalary() < 6000000) {
            return 500000 + getSalary() * 10 / 100;
        } else if (getSalary() < 8000000) {
            return 700000 + getSalary() * 15 / 100;
        } else {
            return 700000 + getSalary() * 17 / 100;
        }
    }

    public double getReceived() {
        return getSalary() - getTax();
    }
}
