package eth.achz;

import eth.achz.app.Application;
import eth.achz.app.util.CommandLineUtils;
import eth.achz.miniapp.*;

import java.util.Scanner;

public class CommandLineInterface {

    public static void main(String[] args) {
        Application app = new Application();
        app = app.init();
        startMenu(app);
    }

    public static void printMenu(String menuTitle) {
        switch (menuTitle) {
            case "start" -> {
                System.out.println("--------------------------------------------");
                System.out.println("| Start Menu");
                System.out.println("|-> 1. load from db.json (recommended!)");
                System.out.println("|-> 2. start from scratch");
            }
            case "main" -> {
                System.out.println("--------------------------------------------");
                System.out.println("| Main menu");
                System.out.println("|-> 1. Students panel");
                System.out.println("|-> 2. Teacher Panel");
                System.out.println("|-> 3. Course Panel");
                System.out.println("|-> 4. mini projects");
            }
            case "student" -> {
                System.out.println("--------------------------------------------");
                System.out.println("| Student menu");
                System.out.println("|-> 1. list all students");
                System.out.println("|-> 2. add student");
                System.out.println("|-> 3. update student");
                System.out.println("|-> 4. search");
                System.out.println("|-> b. back to main menu");
            }
            case "teacher" -> {
                System.out.println("--------------------------------------------");
                System.out.println("| Teacher menu");
                System.out.println("|-> 1. list all teachers");
                System.out.println("|-> 2. add teacher");
                System.out.println("|-> b. back to main menu");
            }
            case "course" -> {
                System.out.println("--------------------------------------------");
                System.out.println("| Course menu");
                System.out.println("|-> 1. list all courses");
                System.out.println("|-> 1. add course");
                System.out.println("|-> b. back to main menu");
            }
            case "miniProjects" -> {
                System.out.println("--------------------------------------------");
                System.out.println("| Mini Projects menu");
                System.out.println("|-> 1. mini project one(import 100 students scores and ...)");
                System.out.println("|-> 2. mini project Two(import 10 students scores and count scores above 16.)");
                System.out.println("|-> 3. mini project Three(import 10 students scores and get min score.)");
                System.out.println("|-> 4. mini project Four(import 10 students scores and get max score.)");
                System.out.println("|-> 5. mini project Five(import 10 students scores and get count of students that passed.)");
                System.out.println("|-> 6. mini project Six(import 10 students scores and get count of students that failed.)");
                System.out.println("|-> 7. mini project Seven(import 10 students scores and get count of students with score equal to 10.)");
                System.out.println("|-> 8. mini project Eight(import employees and ...)");
                System.out.println("|-> b. back to main menu");
            }
            default -> printMenu("main");
        }
    }

    public static void startMenu(Application app) {
        Scanner sc = new Scanner(System.in);
        printMenu("start");
        String command = sc.nextLine();
        switch (command) {
            case "1" -> {
                app.dbWorker.loadFromFile("src/main/resources/db.json");
                System.out.flush();
                CommandLineUtils.printInBox("database loaded!");
                mainMenu(app);
            }
            case "2" -> mainMenu(app);
            default -> {
                CommandLineUtils.invalidInput();
                startMenu(app);
            }
        }
    }


    public static void mainMenu(Application app) {
        Scanner sc = new Scanner(System.in);

        printMenu("main");

        String command = sc.nextLine();
        switch (command) {
            case "1" -> studentMenu(app);
            case "2" -> teacherMenu(app);
            case "3" -> courseMenu(app);
            case "4" -> miniProjectsMenu(app);
            default -> CommandLineUtils.invalidInput();
        }
        mainMenu(app);
    }

    private static void miniProjectsMenu(Application app) {
        Scanner sc = new Scanner(System.in);
        printMenu("miniProjects");
        String command = sc.nextLine();
        switch (command) {
            case "1" -> MiniProjectOne.init();
            case "2" -> MiniProjectTwo.init();
            case "3" -> MiniProjectThree.init();
            case "4" -> MiniProjectFour.init();
            case "5" -> MiniProjectFive.init();
            case "6" -> MiniProjectSix.init();
            case "7" -> MiniProjectSeven.init();
            case "8" -> MiniProjectEight.init();
            case "b" -> mainMenu(app);
            default -> CommandLineUtils.invalidInput();
        }
        miniProjectsMenu(app);
    }

    public static void studentMenu(Application app) {
        Scanner sc = new Scanner(System.in);
        printMenu("student");

        String command = sc.nextLine();
        switch (command) {
            case "1" -> app.studentResource.printAll(); //printAllStudents(eth.achz.app);
            case "2" -> app.studentResource.add();
            case "3" -> app.studentResource.updateById();
            case "4" -> studentSearchMenu(app);
            case "b" -> mainMenu(app);
            default -> CommandLineUtils.invalidInput();
        }
        studentMenu(app);
    }

    private static void studentSearchMenu(Application app) {
        Scanner sc = new Scanner(System.in);

        System.out.println("--------------------------------------------");
        System.out.println("| Student search menu");
        System.out.println("|-> 1. search by id");
        System.out.println("|-> 2. search by student number");
        System.out.println("|-> 3. search by branch");
        System.out.println("|-> 4. search by name");
        System.out.println("|-> b. back to student panel");

        String command = sc.nextLine();
        switch (command) {
            case "1" -> app.studentResource.searchById();
            case "2" -> app.studentResource.searchByStudentNumber();
            case "3" -> app.studentResource.searchByBranch();
            case "4" -> app.studentResource.searchByName();
            case "b" -> System.out.print("");
            default -> CommandLineUtils.invalidInput();
        }
    }

    public static void teacherMenu(Application app) {
        Scanner sc = new Scanner(System.in);
        printMenu("teacher");

        String command = sc.nextLine();
        switch (command) {
            case "1" -> app.teacherResource.printAll();
            case "2" -> app.teacherResource.add();
            case "b" -> mainMenu(app);
            default -> CommandLineUtils.invalidInput();
        }
        teacherMenu(app);
    }

    public static void courseMenu(Application app) {
        Scanner sc = new Scanner(System.in);
        printMenu("course");

        String command = sc.nextLine();
        switch (command) {
            case "1" -> app.courseResource.printAll();
            case "2" -> app.courseResource.add();
            case "b" -> mainMenu(app);
            default -> CommandLineUtils.invalidInput();
        }
        teacherMenu(app);
    }


}
